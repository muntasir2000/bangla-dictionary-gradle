package android.support.v4.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class PagerTabStripFont extends PagerTitleStrip {

	Typeface tf;
	public PagerTabStripFont(Context context) {
		super(context);
		tf = Typeface.createFromAsset(context.getAssets(), "Titillium-Light.otf");
		mCurrText.setTypeface(tf);
		mPrevText.setTypeface(tf);
		mNextText.setTypeface(tf);
		mCurrText.setTextSize((float) 20.0);
	}

	public PagerTabStripFont(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		tf = Typeface.createFromAsset(arg0.getAssets(), "Titillium-Light.otf");
		mCurrText.setTypeface(tf);
		mPrevText.setTypeface(tf);
		mNextText.setTypeface(tf);
		mCurrText.setTextSize((float) 20.0);
	}

}
