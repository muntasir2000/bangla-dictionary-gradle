package com.kaaktaruaa.bn.db;

import android.text.SpannableStringBuilder;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Tareq on 10/5/2014.
 */
public class ParsedResult{
    public String getBnWord() {
        return bnWord;
    }

    public void setBnWord(String bnWord) {
        this.bnWord = bnWord;
    }

    public HashMap<String, List<List<SpannableStringBuilder>>> getDetailsData() {
        return detailsData;
    }

    public void setDetailsData(HashMap<String, List<List<SpannableStringBuilder>>> detailsData) {
        this.detailsData = detailsData;
    }

    private String bnWord;
    private HashMap<String, List<List<SpannableStringBuilder>>> detailsData;
    public ParsedResult(String bnWord, HashMap<String, List<List<SpannableStringBuilder>>> detailsData){
        this.bnWord = bnWord;
        this.detailsData = detailsData;
    }


}

