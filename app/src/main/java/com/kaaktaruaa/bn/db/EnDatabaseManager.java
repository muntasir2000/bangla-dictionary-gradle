package com.kaaktaruaa.bn.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.adapter.DetailsListAdapter;
import com.kaaktaruaa.bn.util.Constants;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Tareq on 10/2/2014.
 */
public class EnDatabaseManager {
    Context ctx;
//    CopyDatabsaeAsyncTask copyDatabsaeAsyncTask;
    ExpandableListView listView;
    TextView bnMeaning;
    AutoCompleteTextView autoCompleteTextView;
    BetterDatabaseHelper dbHelper;
    public EnDatabaseManager(Context ctx, ExpandableListView listView, TextView bnMeaning, AutoCompleteTextView autoCompleteTextView) {
        this.ctx = ctx;
//        copyDatabsaeAsyncTask = new CopyDatabsaeAsyncTask(ctx);
//        copyDatabsaeAsyncTask.execute();
        dbHelper = new BetterDatabaseHelper(ctx);
        this.listView = listView;
        this.bnMeaning = bnMeaning;
        this.autoCompleteTextView = autoCompleteTextView;
    }

    public ArrayList<String> getAutoCompleteSuggestions(String enWord) {
        long starTime = System.currentTimeMillis();
        if (enWord.equals(""))
            return new ArrayList<String>();

        ArrayList<String> suggestions = new ArrayList<String>();
        String sql = "SELECT * FROM EN_IDS " +
                "WHERE EN_WORD LIKE ? " +
                "ORDER BY EN_WORD " +
                ";";

        Log.d(Constants.DEBUG_TAG, "Autocomplete SQL: " + sql);
        SQLiteDatabase database = dbHelper.getReadableDatabase();


        Cursor cursor = null;
        cursor = database.rawQuery(sql, new String[]{enWord.toLowerCase() + "%"});

        while (cursor.moveToNext()) {
            String word = cursor.getString(1);
            suggestions.add(word.toLowerCase());
        }
        if (cursor != null)
            cursor.close();

        Log.i("XXXX AutoComplete latency " , (System.currentTimeMillis() - starTime) + "");
        return suggestions;
    }

    public ParsedResult searchWord(String enWord) {
        enWord = enWord.toLowerCase();
        String sqlIDCheck = "SELECT ID FROM EN_IDS WHERE EN_WORD = '" + enWord + "'";
        String sqlPrimary = "SELECT BN_WORD, EN_DETAILS " +
                "FROM EN_INFO WHERE ID = ? ;";
        String sqlSecondary = "";

        int id = 0;
        String bnWordFromDB = null;
        String enDetailsFromDB = null;


        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursorID = database.rawQuery(sqlIDCheck, null);

       /* Log.d("EnDatabaseManager SearchWrd()", "enWord = " + enWord);
        Log.d("EnDatabaseManager searchWord()", "test");*/

        if (cursorID.moveToNext()) {
            id = cursorID.getInt(0);

        /*    Log.d("EnDatabaseManager searchWord()", "ID = " + id);*/
        }
        cursorID.close();

        Cursor cursorPrimary = database.rawQuery(sqlPrimary, new String[]{id + ""});

        if (cursorPrimary.moveToNext()) {
            bnWordFromDB = cursorPrimary.getString(0);
            enDetailsFromDB = cursorPrimary.getString(1);
        /*    Log.d("EnDatabaseManager SearchWrd()", bnWordFromDB);
            Log.d("EnDatabaseManager searchWord()", enDetailsFromDB);*/
        }
        cursorPrimary.close();

        HashMap<String, List<List<SpannableStringBuilder>>> data = parseDetailsData(enDetailsFromDB);
        ParsedResult parsedResult = new ParsedResult(bnWordFromDB, data);

        addToHistory(id);

        return parsedResult;
    }

    private void addToHistory(int id){
        //add searched word to the history. 'history' is the key of the string stored in sharedpreference
        StringBuilder stringBuilder = new StringBuilder();
        SharedPreferences preferences = ctx.getSharedPreferences(ctx.getPackageName(),0);
        String stringOfWordIdArray = preferences.getString("history", " ");

        if(stringOfWordIdArray.equals(" ")){
            //a user searches a word for the first time after installation
            Log.e("addToHistory" , "else");
            stringBuilder.append(id + ",");
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("history", stringBuilder.toString());
            editor.commit();
            Log.d("Save to history", stringBuilder.toString());
            return;
        }
        String[] arrayOfWordId = stringOfWordIdArray.split(",");


        //check if id already exists in the history. if yes, then delete the old entry so that new entry can be created in the front
        for(int i=0; i<arrayOfWordId.length; i++){
            if(arrayOfWordId[i].equals(id + "")){
                arrayOfWordId[i] = "";
            }
        }

        Log.e("addToHistory", arrayOfWordId.length + "");

        if(arrayOfWordId.length>50){
            //i=1 because we want to delete the oldest word id
            for(int i=1; i<arrayOfWordId.length; i++){
                if(arrayOfWordId[i].equals("")){
                    continue;
                }
                stringBuilder.append(arrayOfWordId[i] + ",");
            }
            stringBuilder.append(id + ",");
            //stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            Log.e("AddToHistory", "array length > 50");
        }else if (arrayOfWordId.length>0){
            for (int i=0; i<arrayOfWordId.length; i++){
                if (arrayOfWordId[i].equals("")){
                    continue;
                }
                stringBuilder.append(arrayOfWordId[i] + ",");
            }

            stringBuilder.append(id + ",");
            //stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            Log.e("AddToHistory", "array length > 0");
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("history", stringBuilder.toString());
        editor.commit();
        Log.d("Save to history", stringBuilder.toString());
    }


    private HashMap<String, List<List<SpannableStringBuilder>>> parseDetailsData(String detailsStr) {
        //check whether the word has a details block or not
        if (detailsStr == null) {
            return new HashMap<String, List<List<SpannableStringBuilder>>>();
        }

        HashMap<String, List<List<SpannableStringBuilder>>> result = new HashMap<String, List<List<SpannableStringBuilder>>>();
        HashMap<Integer, List<List<SpannableStringBuilder>>> resultInteger = new HashMap<Integer, List<List<SpannableStringBuilder>>>();

        ArrayList<ArrayList<Integer>> listIDsWithType = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> listIDs = new ArrayList<Integer>();
        Pattern pattern = Pattern.compile("(\\[(\\d+,)+\\d+\\])+");
        Matcher matcher = pattern.matcher(detailsStr);

        while (matcher.find()) {
            //        System.out.println(matcher.group(0));
            ArrayList<Integer> localListIDWithType = new ArrayList<Integer>();

            Matcher m = Pattern.compile("\\d+").matcher(matcher.group(0));
            while (m.find()) {
                int num = Integer.parseInt(m.group(0));
          //      Log.d("EnDatabaseManager m.group(0) = " ,  m.group(0));
                    System.out.print(m.group(0) + " ");
                if (num < 17) {
                    localListIDWithType.add(num);
                    continue;
                }
                localListIDWithType.add(num);
                listIDs.add(num);
            }
            listIDsWithType.add(localListIDWithType);
            //         System.out.println();
        }

        HashMap<Integer, List<SpannableStringBuilder>> idToDetailsMap = getFromDB(listIDs);

        for (int i = 0; i < listIDsWithType.size(); i++) {


            List<Integer> listIDsSameType = listIDsWithType.get(i);
            List<List<SpannableStringBuilder>> listOfBnENPair = new ArrayList<List<SpannableStringBuilder>>();
            int key = listIDsSameType.get(0);
            //       System.out.println("listIDsSameType = " + listIDsSameType.size());
            for (int j = 1; j < listIDsSameType.size(); j++) {
                int id = listIDsSameType.get(j);
                List<SpannableStringBuilder> bn_EnWord = idToDetailsMap.get(id);

                listOfBnENPair.add(bn_EnWord);
            }
            resultInteger.put(key, listOfBnENPair);

        }


        Set s = resultInteger.keySet();
        Object[] keys = s.toArray();
        for (int i = 0; i < resultInteger.keySet().size(); i++) {
            int key = Integer.parseInt(keys[i].toString());
            List<List<SpannableStringBuilder>> value = resultInteger.get(key);
            switch (key) {
                case 1:
                    break;
                case 2:
                    result.put("Noun", value);
                    break;
                case 3:
                    result.put("Adjective", value);
                    break;
                case 4:
                    result.put("Verb", value);
                    break;
                case 5:
                    result.put("Other", value);
                    break;
                case 6:
                    result.put("Adverb", value);
                    break;
                case 7:
                    result.put("Other", value);
                    break;
                case 8:
                    result.put("Prefix", value);
                    break;
                case 9:
                    result.put("Preposition", value);
                    break;
                case 10:
                    result.put("Pronoun", value);
                    break;
                case 11:
                    result.put("Article", value);
                    break;
                case 12:
                    result.put("Interjection", value);
                    break;
                case 13:
                    result.put("Phrase", value);
                    break;
                case 14:
                    result.put("Suffix", value);
                    break;
                case 15:
                    result.put("Auxiliary Verb", value);
                    break;
                case 16:
                    result.put("Particle", value);

            }
        }

        return result;
    }

    private HashMap<Integer, List<SpannableStringBuilder>> getFromDB(ArrayList<Integer> listIDs) {

        if(listIDs.size()==0){
            Log.e("XXXXXXXXXXXXXXXXXXXX", "listIDs size is zero");
            return new HashMap<Integer, List<SpannableStringBuilder>>();
        }

        HashMap<Integer, List<SpannableStringBuilder>> map = new HashMap<Integer, List<SpannableStringBuilder>>();

        String baseSQL = "SELECT ID, BN_WORD, EN_WORD FROM BN_TO_EN WHERE ";
        int i = 0;
        for (; i < listIDs.size() - 1; i++) {
            baseSQL += " ID = " + listIDs.get(i) + " OR ";
        }
        baseSQL += " ID = " + listIDs.get(i) + ";";

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(baseSQL, null);

        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            SpannableStringBuilder bn = new SpannableStringBuilder(cursor.getString(1));

            List<SpannableStringBuilder> list = new ArrayList<SpannableStringBuilder>();
            list.add(bn);
            list.add(removeQuotesAndAddSpans(cursor.getString(2)));
            map.put(id, list);
        }
        return map;
    }

    private SpannableStringBuilder removeQuotesAndAddSpans(String str) {
    //    Log.d("XXXXXXXXXX from Endatabase removeQuotes", str);
        List<String> wordList = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();

        Pattern pattern = Pattern.compile("((\\w)+\\s*)+");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            String word = matcher.group(0).toLowerCase();
            sb.append(word + ", ");
            wordList.add(word);
        }
        sb.deleteCharAt(sb.lastIndexOf(","));

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(sb.toString());
        int prevIndex = 0;
        for (String word : wordList) {
            int index = sb.indexOf(word, prevIndex);
            stringBuilder.setSpan(new MyClickableSpan(word), index, index + word.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            prevIndex = index + word.length();
        }

       /* int lastIndexOfComma = stringBuilder.toString().lastIndexOf(",");
        stringBuilder.delete(lastIndexOfComma, lastIndexOfComma);*/
        return stringBuilder;
    }

    class MyClickableSpan extends ClickableSpan {
        String word;
        int textColor;

        MyClickableSpan(String word) {
            this.word = word;
            TypedArray ta;
            int[] attrs = {R.attr.clickspanColor};
            ta = ctx.getTheme().obtainStyledAttributes(attrs);
            textColor = ta.getColor(0, Color.GREEN);
            ta.recycle();
        }

        @Override
        public void onClick(View view) {
            Log.d(Constants.DEBUG_TAG, "Click Detected XXXXXXXXXXXX");
            autoCompleteTextView.setThreshold(200);
            autoCompleteTextView.setText(word);
            autoCompleteTextView.setThreshold(1);
            ParsedResult result = searchWord(word);
            bnMeaning.setText(result.getBnWord());
            DetailsListAdapter listAdapter = new DetailsListAdapter(ctx, result.getDetailsData());
            listView.setAdapter(listAdapter);
            for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                listView.expandGroup(i);
            }
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            //ds.setColor(Color.BLACK);
            ds.setColor(textColor);

        }
    }
}