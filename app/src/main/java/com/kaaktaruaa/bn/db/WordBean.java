package com.kaaktaruaa.bn.db;

/**
 * Created by Tareq on 10/2/2014.
 * Container for english words with serial numbers. Mainly used in autocomplete suggestion.
 */
public class WordBean {
    private int serial;
    private String word;
    public WordBean(){

    }
    public WordBean(int serial, String word){
        this.serial = serial;
        this.word = word;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
