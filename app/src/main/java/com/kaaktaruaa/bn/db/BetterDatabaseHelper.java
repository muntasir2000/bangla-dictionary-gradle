package com.kaaktaruaa.bn.db;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.util.Constants;
import com.kaaktaruaa.bn.util.FileHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Tareq on 12/21/2014.
 */
public class BetterDatabaseHelper extends SQLiteOpenHelper {
    private static String DB_DIR = "/data/data/com.kaaktaruaa.bn/databases/";
    private static String DB_NAME = "dictionary.db";
    private static String DB_PATH = DB_DIR + DB_NAME;
    private static String OLD_DB_PATH = DB_DIR + "old_" + DB_NAME;

    private final Context myContext;

    private boolean createDatabase = false;
    private boolean upgradeDatabase = false;

    private static final String TAG = BetterDatabaseHelper.class.getName();
    private  ProgressDialog progressDialog;
    public BetterDatabaseHelper(Context context) {
        super(context, DB_NAME, null, Constants.DB_VERSION);
        myContext = context;
        // Get the path of the database that is based on the context.
        DB_PATH = myContext.getDatabasePath(DB_NAME).getAbsolutePath();
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createDatabase = true;
    }



    public void initializeDataBase(Activity activity) {
        getWritableDatabase();


        Log.e(TAG, "start of init database");

        if (createDatabase || upgradeDatabase) {
            try {
                copyDataBase(activity);
            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("Error copying database");
            }
        }
    }



    private void copyDataBase(final Activity activity) throws IOException {
        Log.e(TAG, "start of COPY database");
        progressDialog = new ProgressDialog(activity, ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Preparing for the first time");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        final InputStream myInput = myContext.getAssets().open(DB_NAME);
        final OutputStream myOutput = new FileOutputStream(DB_PATH);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    FileHelper.copyFile(myInput, myOutput);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(myContext, "Copying done", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();

        Log.e(TAG, "Copying done");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        upgradeDatabase = true;

    }
}
