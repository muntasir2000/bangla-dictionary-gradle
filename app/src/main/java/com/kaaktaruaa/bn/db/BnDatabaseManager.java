package com.kaaktaruaa.bn.db;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import com.kaaktaruaa.bn.MainActivity;
import com.kaaktaruaa.bn.OnClickEnglishSpanCallBack;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.fragment.English2BanglaFragment;
import com.kaaktaruaa.bn.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tareq on 10/5/2014.
 */
public class BnDatabaseManager {

    Context ctx;
    BetterDatabaseHelper dbHelper;
    AutoCompleteTextView autoCompleteTextView;
    FragmentTransaction fragmentTransaction;

    public BnDatabaseManager(Context ctx, FragmentTransaction fragmentTransaction) {
        this.ctx = ctx;
        dbHelper = new BetterDatabaseHelper(ctx);
//        dbHelper.initializeDataBase();
        this.autoCompleteTextView = autoCompleteTextView;
        this.fragmentTransaction = fragmentTransaction;
    }

    public ArrayList<String> getAutoCompleteSuggestions(String enWord) {
        if (enWord.equals(""))
            return new ArrayList<String>();

        ArrayList<String> suggestions = new ArrayList<String>();
        String sql = "SELECT * FROM BN_TO_EN " +
                "WHERE BN_WORD LIKE ? " +
                "ORDER BY BN_WORD " +
                "LIMIT 30;";

        Log.d(Constants.DEBUG_TAG, "Autocomplete SQL: " + sql);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = null;
        cursor = database.rawQuery(sql, new String[]{enWord + "%"});

        while (cursor.moveToNext()) {
            String word = cursor.getString(1);
            suggestions.add(word);
        }
        if (cursor != null)
            cursor.close();

        for (String s : suggestions) {
            Log.d(Constants.DEBUG_TAG, s);
        }

        return suggestions;
    }

    public SpannableStringBuilder searchWord(String bnWord) {
        String result = null;
        String sql = "SELECT EN_WORD FROM BN_TO_EN WHERE BN_WORD = '" + bnWord + "';";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToNext()) {
            result = cursor.getString(0);
        }
        Log.d(Constants.DEBUG_TAG, "Word result " + result);
        return removeQuotesAndAddSpans(result);
    }


    private SpannableStringBuilder removeQuotesAndAddSpans(String str) {

        List<String> wordList = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();

        Pattern pattern = Pattern.compile("((\\w)+\\s*)+");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            String word = matcher.group(0).toLowerCase();
            sb.append(word + ", ");
            wordList.add(word);
        }
        sb.deleteCharAt(sb.lastIndexOf(","));

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(sb.toString());
        int prevIndex = 0;
        for (String word : wordList) {
            int index = sb.indexOf(word, prevIndex);

            stringBuilder.setSpan(new MyClickableSpan(word), index, index + word.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            //    Log.d(Constants.DEBUG_TAG, "ClickableSpan index = " + index + " end index = " + (index+word.length()));
            prevIndex = index + word.length();
        }

        //    Log.d(Constants.DEBUG_TAG, "SpannableStringBuilder length = " + stringBuilder.length());

        return stringBuilder;
    }

    private void callFragment(String word) {
        MainActivity.viewPager.setCurrentItem(0, true);
        OnClickEnglishSpanCallBack fragment = English2BanglaFragment.onClickEnglishSpanCallBack;
        fragment.handle(word);
    }

    private class MyClickableSpan extends ClickableSpan {
        String word;
        int textColor;

        public MyClickableSpan(String word) {
            this.word = word;
            TypedArray ta;
            int[] attrs = {R.attr.clickspanColor};
            ta = ctx.getTheme().obtainStyledAttributes(attrs);
            textColor = ta.getColor(0, Color.GREEN);
            ta.recycle();
        }

        @Override
        public void onClick(View view) {
            //     Log.d(Constants.DEBUG_TAG, "OnClick clickablespan");
            callFragment(word);
            //   MainActivity.viewPager.setCurrentItem(0, true);

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(textColor);
        }
    }
}
