package com.kaaktaruaa.bn.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.util.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tareq on 10/3/2014.
 */
public class DetailsListAdapter extends BaseExpandableListAdapter {
    List<String> dataHeader;
    HashMap<String, List<List<SpannableStringBuilder>>> dataChild;
    Context context;
    LayoutInflater inflater;

    public DetailsListAdapter(Context context, HashMap<String, List<List<SpannableStringBuilder>>> data) {
        this.context = context;
        dataHeader = extractHeaders(data);
        dataChild = data;
        inflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return dataHeader.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return dataChild.get(dataHeader.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return dataHeader.get(i);
    }

    @Override
    public Object getChild(int groupPos, int childPos) {
        return dataChild.get(dataHeader.get(groupPos)).get(childPos);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = inflater.inflate(R.layout.exp_listview_groupheader, null);
        }
        TextView tvGroupHeader = (TextView) convertView.findViewById(R.id.exp_listview_group_header_textview);
        tvGroupHeader.setText(getGroup(groupPosition).toString());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.exp_listview_item, null);
        }

        List<SpannableStringBuilder> child = (List<SpannableStringBuilder>) getChild(groupPosition, childPosition);
        TextView tvBangla = (TextView) convertView.findViewById(R.id.exp_listview_item_textview_bn);
        TextView tvMeaning = (TextView) convertView.findViewById(R.id.exp_listview_item_textview_meaning);

        if(child.get(1) instanceof SpannableStringBuilder){
         //   Toast.makeText(context, "Span" , Toast.LENGTH_SHORT).show();
        //    Log.d(Constants.DEBUG_TAG, child.get(1).toString());
     //       SpannableStringBuilder builder = child.get(1);
         //   ClickableSpan[] spans = builder.getSpans(0, builder.length(), ClickableSpan.class);

        }
        tvBangla.setText(child.get(0));
        tvMeaning.setText((SpannableStringBuilder)child.get(1));
        tvMeaning.setMovementMethod(LinkMovementMethod.getInstance());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    private ArrayList<String> extractHeaders(HashMap<String, List<List<SpannableStringBuilder>>> data) {
        ArrayList<String> list = new ArrayList<String>();
        Object[] array = data.keySet().toArray();
        for (Object ob : array) {
            list.add(ob.toString());
        }
        return list;
    }

}
