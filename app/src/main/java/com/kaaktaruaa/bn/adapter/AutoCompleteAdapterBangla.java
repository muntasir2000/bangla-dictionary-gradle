package com.kaaktaruaa.bn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.kaaktaruaa.bn.R;

import java.util.ArrayList;

/**
 * Created by Tareq on 10/5/2014.
 */
public class AutoCompleteAdapterBangla extends BaseAdapter implements Filterable {

    private LayoutInflater mLayoutInflater;
    ArrayList<String> data;
    Context context;

    public AutoCompleteAdapterBangla(Context context, ArrayList<String> data){
        this.data = data;
        this.context = context;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.autocomplete_dropdown_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tvAutoCompleteItem);
        tv.setText(data.get(pos));
        return convertView;
    }

    WordFilter wordFilter;
    @Override
    public Filter getFilter() {
        if(wordFilter == null){
            wordFilter = new WordFilter(data);
        }

        return wordFilter;
    }


    private class WordFilter extends Filter {
        ArrayList<String> data;
        public WordFilter(ArrayList<String> data){
            this.data = data;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(data == null)
                return results;
            // get your cursor by passing appropriate query here
            results.values = data;
            results.count = data.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    }
}
