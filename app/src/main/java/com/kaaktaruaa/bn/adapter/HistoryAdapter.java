package com.kaaktaruaa.bn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.util.HistoryItem;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 12/15/14.
 */
public class HistoryAdapter extends ArrayAdapter<HistoryItem> {
    LayoutInflater inflater;
    List<HistoryItem> listOfHistoryItem;
    public HistoryAdapter(Context context, int resource, List<HistoryItem> objects) {
        super(context, resource, objects);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listOfHistoryItem = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.history_item, null);
            holder = new ViewHolder();
            holder.tvWord = (TextView) convertView.findViewById(R.id.tvWord);
            holder.tvMeaning = (TextView) convertView.findViewById(R.id.tvMeaning);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxHistoryItem);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckBox cb = (CheckBox) view;
                    HistoryItem historyItem = (HistoryItem) cb.getTag();
                    historyItem.isSelected = cb.isChecked();
                }
            });
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        HistoryItem item = listOfHistoryItem.get(position);
        holder.tvWord.setText(item.word);
        holder.tvMeaning.setText(item.meaning);
        holder.checkBox.setChecked(item.isSelected);
        holder.checkBox.setTag(item);

        return convertView;
    }

    class ViewHolder{
        TextView tvWord;
        TextView tvMeaning;
        CheckBox checkBox;
    }
}
