package com.kaaktaruaa.bn.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.db.WordBean;
import com.kaaktaruaa.bn.util.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tareq on 10/3/2014.
 */
public class AutoCompleteAdapterEnglish extends BaseAdapter implements Filterable{

    private  LayoutInflater mLayoutInflater;
    ArrayList<String> data;
    Context context;

    public AutoCompleteAdapterEnglish(Context context, ArrayList<String> data){
        this.data = data;
        this.context = context;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(data != null) {
            return data.size();
        }else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        if(data != null) {
            return data.get(i);
        }else
            return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.autocomplete_dropdown_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tvAutoCompleteItem);
        tv.setText(data.get(pos));


        return convertView;
    }
    WordFilter wordFilter;
    @Override
    public Filter getFilter() {
        if(wordFilter == null){
            wordFilter = new WordFilter(data);
        }

        return wordFilter;
    }


    private class WordFilter extends Filter {
        ArrayList<String> data;
        public WordFilter(ArrayList<String> data){
            this.data = data;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(data == null)
                return results;
            // get your cursor by passing appropriate query here
            results.values = data;
            results.count = data.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    }
}
