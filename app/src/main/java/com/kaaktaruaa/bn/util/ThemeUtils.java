package com.kaaktaruaa.bn.util;

/**
 * Created by Tareq on 11/8/2014.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import com.kaaktaruaa.bn.R;


public class ThemeUtils {

    public final static int BLACK = 0;
    public final static int BLUE = 1;
    private static int cTheme;

    public static void changeToTheme(Activity activity, int theme, DialogInterface dialogInterface) {
        cTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.setTheme(theme);
        dialogInterface.dismiss();
    }

    public static void onActivityCreateSetTheme(Activity activity, int theme) {
        switch (theme) {
            default:
            case R.style.DarkTheme:
                activity.setTheme(R.style.DarkTheme);
                break;
            case R.style.SOTheme:
                activity.setTheme(R.style.SOTheme);
                break;
            case R.style.NotepadTheme:
                activity.setTheme(R.style.NotepadTheme);
                break;
            case R.style.PurpleTheme:
                activity.setTheme(R.style.PurpleTheme);
                break;
            case R.style.WasabiTheme:
                activity.setTheme(R.style.WasabiTheme);
        }
    }
}