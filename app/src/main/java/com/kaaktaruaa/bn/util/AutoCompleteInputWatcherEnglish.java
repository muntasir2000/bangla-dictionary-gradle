package com.kaaktaruaa.bn.util;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import com.kaaktaruaa.bn.DataLoaderEnglish;
import com.kaaktaruaa.bn.adapter.AutoCompleteAdapterEnglish;
import com.kaaktaruaa.bn.db.EnDatabaseManager;

/**
 * Created by Tareq on 10/3/2014.
 */
public class AutoCompleteInputWatcherEnglish implements TextWatcher {
    AutoCompleteTextView textView;
    EnDatabaseManager databaseManager;
    AutoCompleteAdapterEnglish adapter;


    public AutoCompleteInputWatcherEnglish(Context context, AutoCompleteTextView textView, EnDatabaseManager db){
        this.textView = textView;
        databaseManager = db;
        adapter = new AutoCompleteAdapterEnglish(context, null);
        textView.setAdapter(adapter);
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        /*data = databaseManager.getAutoCompleteSuggestions(charSequence.toString());
        for (WordBean wordBean: data){
            Log.d(Constants.DEBUG_TAG, wordBean.getWord());
        }
        adapter.setData(data);*/
        DataLoaderEnglish dataLoader = new DataLoaderEnglish(databaseManager, adapter);
        dataLoader.execute(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
