package com.kaaktaruaa.bn.util;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import com.kaaktaruaa.bn.adapter.AutoCompleteAdapterBangla;
import com.kaaktaruaa.bn.db.BnDatabaseManager;

import java.util.ArrayList;

/**
 * Created by Tareq on 10/5/2014.
 */
public class AutoCompleteInputWatcherBangla implements TextWatcher {
    AutoCompleteTextView textView;
    BnDatabaseManager databaseManager;
    AutoCompleteAdapterBangla adapter;

    public AutoCompleteInputWatcherBangla(Context context, AutoCompleteTextView textView, FragmentTransaction fragmentTransaction){
        this.textView = textView;
        databaseManager = new BnDatabaseManager(context, fragmentTransaction);
        adapter = new AutoCompleteAdapterBangla(context, new ArrayList<String>());
        textView.setAdapter(adapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        DataLoaderBangla dataLoader = new DataLoaderBangla(databaseManager, adapter);
        dataLoader.execute(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private class DataLoaderBangla  extends AsyncTask<String, Void, ArrayList<String>>{
        BnDatabaseManager databaseManager;
        AutoCompleteAdapterBangla adapter;

        public DataLoaderBangla(BnDatabaseManager databaseManager, AutoCompleteAdapterBangla adapter){
            this.databaseManager = databaseManager;
            this.adapter = adapter;

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<String> wordBeans) {
            if(wordBeans == null){
                Log.d(Constants.DEBUG_TAG, "Wordbean NULLLLLLLLLLLLLL");
            }
            adapter.setData(wordBeans);
        }

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            return databaseManager.getAutoCompleteSuggestions(strings[0]);
        }
    }
}
