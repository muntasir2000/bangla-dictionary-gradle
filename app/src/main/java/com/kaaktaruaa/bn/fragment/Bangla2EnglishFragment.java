package com.kaaktaruaa.bn.fragment;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.adapter.EmptyExpandableListAdapter;
import com.kaaktaruaa.bn.db.BnDatabaseManager;
import com.kaaktaruaa.bn.util.AutoCompleteInputWatcherBangla;

import java.util.Locale;


/**
 * Created by Tareq on 9/13/2014.
 */
public class Bangla2EnglishFragment extends Fragment implements AdapterView.OnItemClickListener{
    AutoCompleteTextView searchBox;
    TextView result;
    BnDatabaseManager databaseManager;
    InputMethodManager inputMethodManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bn2en_fragment, container, false);
        searchBox = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTvBangla);
        result = (TextView) v.findViewById(R.id.textViewBanglaDetails);

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        databaseManager = new BnDatabaseManager(getActivity(),  fragmentTransaction);

        searchBox.addTextChangedListener(new AutoCompleteInputWatcherBangla(getActivity(), searchBox, fragmentTransaction));
        searchBox.setOnItemClickListener(this);
        searchBox.setThreshold(1);
        inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        return v;
    }



    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        TextView tx =(TextView)view.findViewById(R.id.tvAutoCompleteItem);
        String s = tx.getText().toString();
   //     Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
        result.setText((SpannableStringBuilder)databaseManager.searchWord(s));
        result.setMovementMethod(LinkMovementMethod.getInstance());
        //hide the keyboard after clicking a autosuggestion item
        inputMethodManager.hideSoftInputFromWindow(searchBox.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_bangla_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_clear_bn){
            searchBox.setText("");
            result.setText("");

        }
        return super.onOptionsItemSelected(item);
    }


}
