package com.kaaktaruaa.bn.fragment;


import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.kaaktaruaa.bn.OnClickEnglishSpanCallBack;
import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.adapter.DetailsListAdapter;
import com.kaaktaruaa.bn.adapter.EmptyExpandableListAdapter;
import com.kaaktaruaa.bn.db.EnDatabaseManager;
import com.kaaktaruaa.bn.db.ParsedResult;
import com.kaaktaruaa.bn.util.AutoCompleteInputWatcherEnglish;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * Created by Tareq on 9/13/2014.
 */
public class English2BanglaFragment extends Fragment implements AdapterView.OnItemClickListener, OnClickEnglishSpanCallBack, TextToSpeech.OnInitListener {

    public static OnClickEnglishSpanCallBack onClickEnglishSpanCallBack;
    HashMap<String, List<List<String>>> data;
    AutoCompleteTextView searchBox;
    ExpandableListView listView;
    EnDatabaseManager databaseManager;
    TextView bnMeaning;
    InputMethodManager inputMethodManager;
    ImageButton btnSpeak;
    TextToSpeech tts;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        tts = new TextToSpeech(getActivity(), this);
    }

    @Override
    public void onDestroy() {

        if (tts != null)
            tts.shutdown();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.en2bn_fragment, container, false);
        onClickEnglishSpanCallBack = this;
        searchBox = (AutoCompleteTextView)
                v.findViewById(R.id.autoCompleteSearchEnglish);
        listView = (ExpandableListView)
                v.findViewById(R.id.expandableListView);
        bnMeaning = (TextView) v.findViewById(R.id.tvBnMeaning);
        btnSpeak = (ImageButton) v.findViewById(R.id.tts_speak);

        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                speak();
            }
        });
        btnSpeak.setVisibility(View.INVISIBLE);

        databaseManager = new EnDatabaseManager(getActivity(), listView, bnMeaning, searchBox);
        searchBox.addTextChangedListener(new AutoCompleteInputWatcherEnglish(getActivity(), searchBox, databaseManager));
        searchBox.setOnItemClickListener(this);
        searchBox.setThreshold(1);

        //     prepareData();

        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_english_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        TextView tx = (TextView) view.findViewById(R.id.tvAutoCompleteItem);
        String s = tx.getText().toString();

        //  Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
        ParsedResult result = databaseManager.searchWord(s);
        bnMeaning.setText(result.getBnWord());

        DetailsListAdapter listAdapter = new DetailsListAdapter(getActivity(), result.getDetailsData());
        listView.setAdapter(listAdapter);
        for (int ii = 0; ii < listAdapter.getGroupCount(); ii++) {
            listView.expandGroup(ii);
        }

        //hide the keyboard after clicking a autosuggestion item
        inputMethodManager.hideSoftInputFromWindow(searchBox.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        btnSpeak.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_clear_en) {
            //     Toast.makeText(getActivity(),"clear clicked", Toast.LENGTH_SHORT).show();
            listView.setAdapter(new EmptyExpandableListAdapter());
            searchBox.setText("");
            bnMeaning.setText("");
            btnSpeak.setVisibility(View.INVISIBLE);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handle(String word) {
        searchBox.setThreshold(100);
        searchBox.setText(word);
        searchBox.setThreshold(1);

        ParsedResult result = databaseManager.searchWord(word);
        bnMeaning.setText(result.getBnWord());

        DetailsListAdapter listAdapter = new DetailsListAdapter(getActivity(), result.getDetailsData());
        listView.setAdapter(listAdapter);
        for (int ii = 0; ii < listAdapter.getGroupCount(); ii++) {
            listView.expandGroup(ii);
        }

        //hide the keyboard after clicking a autosuggestion item
        inputMethodManager.hideSoftInputFromWindow(searchBox.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        btnSpeak.setVisibility(View.VISIBLE);

    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                btnSpeak.setEnabled(true);
                speak();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void speak() {
        String text = searchBox.getText().toString();
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
}
