package com.kaaktaruaa.bn.fragment;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.kaaktaruaa.bn.R;
import com.kaaktaruaa.bn.adapter.HistoryAdapter;
import com.kaaktaruaa.bn.db.BetterDatabaseHelper;
import com.kaaktaruaa.bn.util.HistoryItem;
import com.kaaktaruaa.bn.util.ThemeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 12/15/14.
 */
public class HistoryActivity extends ActionBarActivity {
    HistoryAdapter historyAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = this.getSharedPreferences(this.getPackageName(), MODE_PRIVATE);
        String chosenTheme = preferences.getString("active_theme", "Dark");

        if (chosenTheme.equals("Indigo")) {
            //   setTheme(R.style.SOTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.SOTheme);
        } else if (chosenTheme.equals("Notepad")) {
            //setTheme(R.style.NotepadTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.NotepadTheme);
        } else if (chosenTheme.equals("Dark")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.DarkTheme);
        } else if (chosenTheme.equals("Purple")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.PurpleTheme);
        } else if (chosenTheme.equals("Wasabi Green")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.WasabiTheme);
        }

        setContentView(R.layout.history_activity);
        ListView listView = (ListView) findViewById(R.id.listViewHistoryActivity);
        historyAdapter = new HistoryAdapter(this, R.layout.history_item, getWordsFromHisoryAndDB());
        listView.setAdapter(historyAdapter);

    }

    private List<HistoryItem> getWordsFromHisoryAndDB(){
        List<HistoryItem> list = new ArrayList<HistoryItem>();
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        SharedPreferences preferences = getSharedPreferences(getPackageName(), 0);
        String stringOfWordIdArray = preferences.getString("history", " ");
        String[] arrayOfIDs = stringOfWordIdArray.split(",");
        Log.d("HistoryActivity", "\"" + stringOfWordIdArray + "\"");
     //   stringOfWordIdArray = new StringBuilder(stringOfWordIdArray).deleteCharAt(0).toString();
        if(arrayOfIDs[0].equals("")){
            return new ArrayList<HistoryItem>();
        }

        BetterDatabaseHelper dbHelper = new BetterDatabaseHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String sqlGetWord = "SELECT ID, EN_WORD FROM EN_IDS WHERE ID IN(";
        String sql = "SELECT ID, BN_WORD " +
                "FROM EN_INFO WHERE ID IN( ";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(stringOfWordIdArray);
        stringBuilder.append(");");

        if (stringBuilder.toString().contains(",")){
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
        }
        Log.d("History SQL", stringBuilder.toString());

        //RETRIEVE THE ENGLISH WORDS CORRESPONDING TO THEIR IDS
        Cursor retrieveIDCursor = database.rawQuery(sqlGetWord + stringBuilder.toString(), null);
        while(retrieveIDCursor.moveToNext()){
            map.put(retrieveIDCursor.getInt(0), retrieveIDCursor.getString(1));
        }
        retrieveIDCursor.close();

        Cursor cursor = database.rawQuery(sql + stringBuilder.toString(), null);

        HashMap<Integer, HistoryItem> mapOfHistory = new HashMap<Integer, HistoryItem>();

        while(cursor.moveToNext()){

            HistoryItem item = new HistoryItem();
            item.word = map.get(cursor.getInt(0));
            item.meaning = cursor.getString(1);
            mapOfHistory.put(cursor.getInt(0), item);
        }
        cursor.close();

        for(int i=0; i<arrayOfIDs.length; i++){
            int id = Integer.parseInt(arrayOfIDs[i]);
            HistoryItem historyItem = mapOfHistory.get(id);
            list.add(historyItem);
        }
        //so that recent words appear on top
        Collections.reverse(list);
        return list;
    }

    public void onClickClearButton(View v){
       /* ArrayList<HistoryItem> list = new ArrayList<HistoryItem>();
        for(int i=0; i<historyAdapter.getCount(); i++){
            HistoryItem item = historyAdapter.getItem(i);
            if(!item.isSelected) { // if the item is not marked as to be cleared, we add the item to list
                list.add(item);
            }
        }

        //now generate a string containing ids and an extra comma in the end
        StringBuilder stringBuilder = new StringBuilder();
        for(HistoryItem item: list){
            stringBuilder.append(item.)
        }*/
        SharedPreferences preferences = getSharedPreferences(this.getPackageName(),0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("history", "");
        editor.commit();
        recreate();
    }
}
