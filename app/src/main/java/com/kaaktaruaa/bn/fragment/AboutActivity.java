package com.kaaktaruaa.bn.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.kaaktaruaa.bn.R;

/**
 * Created by Tareq on 11/24/2014.
 */
public class AboutActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_fragment_layout);

       /* TypedArray ta;
        int[] attrs = {R.attr.baseBackgroundColor, R.attr.fontColor};
        ta = getTheme().obtainStyledAttributes(attrs);
        int backgroundColor = ta.getColor(0, Color.RED);
        int textColor = ta.getColor(1, Color.GREEN);
        Toast.makeText(this, "" + textColor, Toast.LENGTH_SHORT).show();
        ta.recycle();*//*

        SharedPreferences preferences = this.getSharedPreferences(this.getPackageName(), MODE_PRIVATE);
        String chosenTheme = preferences.getString("active_theme", "Dark");
        Toast.makeText(this, chosenTheme, Toast.LENGTH_SHORT).show();
        int textColor = 0, backgroundColor = 0;
        if(chosenTheme.contains("Dark")){
            textColor = 0xD3D3D3;
            backgroundColor = 0x222222;
        }else if(chosenTheme.contains("Indigo")){
            textColor = 0x212121;
            backgroundColor = 0xffffff;
        }else if(chosenTheme.contains("Notepad")){
            textColor = 0x212121;
            backgroundColor = 0xf1e3ce;
        }


        TextView tvTitle = (TextView) findViewById(R.id.about_tv_textView_title);
        TextView tvDetails = (TextView) findViewById(R.id.about_tv_textView_details);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.about_layout_id);

        tvTitle.setTextColor(textColor);
        tvDetails.setTextColor(textColor);
        relativeLayout.setBackgroundColor(backgroundColor);

        Log.d("XXXXXXXX", "About Activity oncreate");*/
    }
}
