package com.kaaktaruaa.bn;

import android.os.AsyncTask;
import com.kaaktaruaa.bn.adapter.AutoCompleteAdapterEnglish;
import com.kaaktaruaa.bn.db.EnDatabaseManager;
import com.kaaktaruaa.bn.db.WordBean;

import java.util.ArrayList;

/**
 * Created by Tareq on 10/3/2014.
 */
public class DataLoaderEnglish extends AsyncTask<String, Void, ArrayList<String>> {
    private EnDatabaseManager databaseManager;
    private AutoCompleteAdapterEnglish adapter;
    public DataLoaderEnglish(EnDatabaseManager databaseManager, AutoCompleteAdapterEnglish adapter){
        this.databaseManager = databaseManager;
        this.adapter = adapter;

    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<String> wordBeans) {
        adapter.setData(wordBeans);
    }

    @Override
    protected ArrayList<String> doInBackground(String... strings) {
        return databaseManager.getAutoCompleteSuggestions(strings[0]);
    }
}
