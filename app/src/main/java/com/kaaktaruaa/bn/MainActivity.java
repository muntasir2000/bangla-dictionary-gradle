package com.kaaktaruaa.bn;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kaaktaruaa.bn.db.BetterDatabaseHelper;
import com.kaaktaruaa.bn.fragment.AboutActivity;
import com.kaaktaruaa.bn.fragment.HistoryActivity;
import com.kaaktaruaa.bn.fragment.MyFragmentPagerAdapter;
import com.kaaktaruaa.bn.util.ThemeUtils;

import java.lang.reflect.Field;

public class MainActivity extends ActionBarActivity {
    public static ViewPager viewPager;
    SharedPreferences preferences;
    AlertDialog.Builder themeChoosingDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        preferences = this.getSharedPreferences(this.getPackageName(), MODE_PRIVATE);
        String chosenTheme = preferences.getString("active_theme", "Purple");
        Log.d("MainActivity", chosenTheme);

        if (chosenTheme.equals("Indigo")) {
            //   setTheme(R.style.SOTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.SOTheme);
        } else if (chosenTheme.equals("Notepad")) {
            //setTheme(R.style.NotepadTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.NotepadTheme);
        } else if (chosenTheme.equals("Dark")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.DarkTheme);
        } else if (chosenTheme.equals("Purple")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.PurpleTheme);
        } else if (chosenTheme.equals("Wasabi Green")) {
            //setTheme(R.style.DarkTheme);
            ThemeUtils.onActivityCreateSetTheme(this, R.style.WasabiTheme);
        }
        //   ThemeUtils.onActivityCreateSetTheme(this, R.style.DarkTheme);

        setContentView(R.layout.main);

        BetterDatabaseHelper databaseHelper = new BetterDatabaseHelper(this);
        databaseHelper.initializeDataBase(this);
/*
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(fragmentManager);
        viewPager.setAdapter(adapter);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menus, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.action_theme_choose) {
            final String[] themes = {"Indigo", "Notepad", "Dark", "Purple", "Wasabi Green"};
            SharedPreferences preferences = this.getSharedPreferences(this.getPackageName(), MODE_PRIVATE);
            final SharedPreferences.Editor editor = preferences.edit();

            themeChoosingDialog = new AlertDialog.Builder(this);
            themeChoosingDialog.setTitle("Choose your theme");
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, themes);
            themeChoosingDialog.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    editor.putString("active_theme", themes[i].toString());
                    editor.commit();
                    if (themes[i].toString().equals("Indigo")) {
                        //      setTheme(R.style.SOTheme);
                        ThemeUtils.changeToTheme(MainActivity.this, R.style.SOTheme, dialogInterface);
                    } else if (themes[i].toString().equals("Notepad")) {
                        //     setTheme(R.style.NotepadTheme);
                        ThemeUtils.changeToTheme(MainActivity.this, R.style.NotepadTheme, dialogInterface);
                    } else if (themes[i].toString().equals("Dark")) {
                        //setTheme(R.style.DarkTheme);
                        ThemeUtils.changeToTheme(MainActivity.this, R.style.DarkTheme, dialogInterface);
                    }else if (themes[i].toString().equals("Purple")) {
                        //setTheme(R.style.DarkTheme);
                        ThemeUtils.changeToTheme(MainActivity.this, R.style.PurpleTheme, dialogInterface);
                    }else if (themes[i].toString().equals("Wasabi Green")) {
                        //setTheme(R.style.DarkTheme);
                        ThemeUtils.changeToTheme(MainActivity.this, R.style.WasabiTheme, dialogInterface);
                    }
                }
            });

            themeChoosingDialog.show();
        }else if(item.getItemId() == R.id.action_history){
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public static class PlaceholderFragment extends Fragment {
        private FragmentActivity fragmentActivity;
        public PlaceholderFragment() {

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            fragmentActivity = (FragmentActivity) activity;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.main_fragment_layout, container, false);

            viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
            android.support.v4.app.FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
            MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(fragmentManager);
            viewPager.setAdapter(adapter);

            return rootView;
        }

        /**
         * This class makes the ad request and loads the ad.
         */
        public static class AdFragment extends Fragment {

            private AdView mAdView;

            public AdFragment() {
            }

            @Override
            public void onActivityCreated(Bundle bundle) {
                super.onActivityCreated(bundle);

                // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
                // values/strings.xml.
                mAdView = (AdView) getView().findViewById(R.id.adView);
          //      mAdView.setAdSize(AdSize.BANNER);

                // Create an ad request. Check logcat output for the hashed device ID to
                // get test ads on a physical device. e.g.
                // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();

                // Start loading the ad in the background.
                mAdView.loadAd(adRequest);
            }

            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                return inflater.inflate(R.layout.fragment_ad, container, false);
            }

            /** Called when leaving the activity */
            @Override
            public void onPause() {
                if (mAdView != null) {
                    mAdView.pause();
                }
                super.onPause();
            }

            /** Called when returning to the activity */
            @Override
            public void onResume() {
                super.onResume();
                if (mAdView != null) {
                    mAdView.resume();
                }
            }

            /** Called before the activity is destroyed */
            @Override
            public void onDestroy() {
                if (mAdView != null) {
                    mAdView.destroy();
                }
                super.onDestroy();
            }

        }
    }


}
